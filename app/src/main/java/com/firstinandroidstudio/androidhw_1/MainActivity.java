package com.firstinandroidstudio.androidhw_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;


public class MainActivity extends AppCompatActivity {
    private Button btn;
    private TextView userName;
    private TextView pass;

    @BindView(R.id.login_btn)Button lBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



    }


    @OnClick(R.id.login_btn)
    public void logIn(View view){
        Intent homePage=new Intent(MainActivity.this,HomePage.class);
        startActivity(homePage);

    }



    /*public void logIn(){
        btn=(Button)findViewById(R.id.login_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homePage=new Intent(MainActivity.this,HomePage.class);
                startActivity(homePage);
            }
        });
    }*/

    //or
    /*
    * public void logIn(View view){
                Intent homePage=new Intent(MainActivity.this,HomePage.class);
                startActivity(homePage);
    }

 */


}
